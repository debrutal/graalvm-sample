#!/usr/bin/env bash
set -ex

mvn   install

$GRAALVM_HOME/bin/native-image -cp ./target/mixed-code-hello-world-1.0-SNAPSHOT.jar -H:Name=kotlinHello -H:Class=hello.KotlinHello -H:+ReportUnsupportedElementsAtRuntime --allow-incomplete-classpath
