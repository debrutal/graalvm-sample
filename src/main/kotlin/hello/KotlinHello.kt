package hello

import java.io.IOException
import java.util.concurrent.TimeUnit

object KotlinHello {
    @JvmStatic
    fun main(args: Array<String>) {
        val response = """psql --version""".runCommandWithResult()
        println(response)
    }

}


fun String.runCommandWithResult(): String? {
    return try {
        val parts = this.split("\\s".toRegex())
        val proc = ProcessBuilder(*parts.toTypedArray())
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .start()

        proc.waitFor(60, TimeUnit.MINUTES)
        proc.inputStream.bufferedReader().readText()
    } catch (e: IOException) {
        e.printStackTrace()
        "error " + e.message
    }
}
